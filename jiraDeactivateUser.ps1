﻿$list = get-content c:\tmp\jiraUserListDeactivate.txt
$jiraUrl = "https://jira.server.com/rest/api/2/user"
$credential = (get-credential)

foreach ($user in $list) {
    $jsonData = "{
        ""name"":""${user}"",
        ""active"":false
        }"
    
    invoke-webrequest -uri "${jiraUrl}?username=${user}" -contentType "application/json" -Method Put -credential $credential -authentication basic -body $jsonData

}
