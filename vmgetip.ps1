param(
	$userip,
	$pipe
)

#REGION VAR
$viServer = "localhost"
$iplist = ""
#ENDREGION

#REGION Functions
function searchIP($ipaddress) {

}
#ENDREGION

if ($pipe -eq $true){
	write-host "Pipe activated"
}

if (!($userip)) {
	$userip = read-host "Your search IP or Path to an IP List File (you can use .\vmgetIP.ps1 xxx.xxx.xxx.xxx or C:\temp\iplist.txt)"
}

if (test-path $userip) {
	$iplist = get-content $userip 
	write-host "File Detected"
} else {
	$iplist = $userip
	write-Host "Manual IP Detected"
}

if (!($connected)) { $connected = connect-viserver $viServer}
$vms = get-vm
foreach ($vm in $vms) {
	$ips = (get-vmguest $vm).IpAddress
	foreach ($ip in $ips) {
		foreach ($userip in $iplist) {
			if ($ip -eq $userip ) {
				if ($pipe) {
					write-output $vm.name
				} else {
					write-host $vm.name " - " $ip
				}
			}
		}
	}
}