$oldDomain = "old.com"
$newDomain = "0.onmicrosoft.com"

connect-AzureAD 
$users = Get-azureadUser -all $true| where { $_.userPrincipalName -like "*"+$oldDomain }
foreach ($user in $users) {
    $shortUser = ($user.mail).split("@")[0]
    $oldUPN = $shortUser + "@" + $oldDomain
    $newUPN = $shortUser + "@" + $newDomain
    set-azureaduser -ObjectId $oldUpn -UserPrincipalName $newUPN
} 
