$servers = ("List","of","servers)
$pass = ConvertTo-SecureString -string "YourPassword" -AsPlainText -Force

foreach ($server in $servers)
{
	$cert = get-certificate -template "Templatename (from certutil -template)" -subjectname "cn=${server}" -dnsname ${server} -CertStoreLocation "cert://LocalMachine/MY"
	export-pfxcertificate $cert.certificate -password $pass "C:\temp\${server}_sccm.pfx" 
	remove-item $cert.certificate.pspath
}
