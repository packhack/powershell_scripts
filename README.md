## Several Powershell Scripts

# vmgetip.ps1 - search a VM when you have only the IP

*Usage:*
---
./vmgetip.ps1 ipaddress -> search for ip address

./vmgetip.ps1 C:\tmp\iplist.txt -> search for ip addresses from text file (multiple)

./vmgetip.ps1 -pipe $true ipaddress | C:\tmp\iplist.txt -> search for ip addresses and give output without ipaddress for piping to other commands


# jiradeactivateuser.ps1 - deactive a list of users in a jira instance (Jira 8+)

*Usage:*
---
./jiradeactiveuser.ps1 -> deactivate user from the $list File